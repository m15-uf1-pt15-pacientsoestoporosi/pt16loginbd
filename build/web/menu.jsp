<nav class="navbar sticky-top navbar-light bg-light">
    <a class="navbar-brand" href="Books?action=List">
        <img src="images/icon-bone.png" width="72" height="72" class="d-inline-block align-middle" alt="icon-bone">
       ESTUDI PACIENTS OESTOPOROSI 
    </a>
    <nav class="nav nav-pills flex-column flex-sm-row">
        <%
            StringBuilder links = new StringBuilder();
                // Llistat pacients. 
                links.append("<a class='flex-sm-fill text-sm-center nav-link' href='Books?action=List'>Llistat Llibres</a>");
                links.append("<a class='flex-sm-fill text-sm-center nav-link' href='Patients?action=List'>Llistat Pacients</a>");
                links.append("<a class='flex-sm-fill text-sm-center nav-link' href='Patients?action=FilterForm'>Filtre Pacients</a>");
                // links.append("<a class='flex-sm-fill text-sm-center nav-link' href='./grafic.jsp'>Gr�fic</a>");
                links.append("<a class='flex-sm-fill text-sm-center nav-link' href='Patients?action=graficPatients'>Nou Gr�fic</a>");
                
                if (session.getAttribute("role").equals("ADMIN")) {
                    links.append("<a class='flex-sm-fill text-sm-center nav-link' href='user?action=AdminUsers'>Gesti� usuaris</a>");
                }
                links.append("<a class='flex-sm-fill text-sm-center nav-link disabled' href='user?action=Invalidate'>Logout</a>");
            out.println(links.toString());
        %>
    </nav>
</nav>
