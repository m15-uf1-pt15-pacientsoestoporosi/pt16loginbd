<%-- 
    Document   : welcome
    Created on : Dec 19, 2018, 4:17:45 PM
    Author     : alumne
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="java.util.*" %>
<%@ page import="model.Book" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Boostrap CSS only -->
        <!--
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous"> -->
        <!--VENDORS-->
        <link rel="stylesheet" href="vendors/bootstrap-4.1.3-dist/css/bootstrap.min.css">
        <script src="vendors/jquery/jquery-3.3.1.min.js"></script>
        <!--CONTROL-->
        <script type="text/javascript" src="./js/control/utils.js"></script>
        <script type="text/javascript" src="./js/control/index.js"></script>
        <title>PORTAL PT152.</title>
    </head>
    <body>
        <%  
            List<Book> booksList = new ArrayList<Book>();
            if(session.getAttribute("user")==null){
                response.sendRedirect("login.jsp");
            } else {
                // Carga la lista de libros.
                if(request.getAttribute("booksList")!=null) {
                    booksList = (List<Book>) request.getAttribute("booksList");
                    // out.println(books.size());
                } else {
                    // Cridem al servlet de Llibres, mètode de carrega.
                    response.sendRedirect("Books?action=List");
                }
        %>
        <header>
            <%@include file="menu.jsp" %>
        </header>
        <main>
           <table class="table table-striped">    
                <thead>
                <th scope='col'>Isbn</th>
                <th scope='col'>Títol</th>
                <th scope='col'>Categoria</th>
                <th scope='col'>Format</th>
                </thead>
            <%
                for(Book book: booksList) {
            %>
                <tr>
                    <td scope="row" width="20%">
                        <%=book.getIsbn()%>
                    </td>
                    <td width="40%">
                        <%=book.getTitulo()%>
                    </td>
                    <td width="10%">
                        <%=book.getCategoria()%>
                    </td>
                    <td width="10%">
                        <%=book.getFormato()%>
                    </td>
                </tr>
            <%
                }
              }
            %>  
            </table>
        </main>
        <footer>
               
        </footer>
    </body>
</html>
