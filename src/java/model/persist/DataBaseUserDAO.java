package model.persist;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Properties;

import model.User;

public class DataBaseUserDAO {

    private static DBConnect dataSource;
    private final Properties queries;
    private static String PROPS_FILE;
    

    public DataBaseUserDAO(String ruta) throws IOException {
        queries = new Properties();
        PROPS_FILE = ruta + "/user_queries.properties";
        queries.load(new FileInputStream(PROPS_FILE));

        dataSource = DBConnect.getInstance();
    }

    public String getQuery(String queryName) {
        return queries.getProperty(queryName);
    }

    public ArrayList<User> findAll() {
        ArrayList<User> list = new ArrayList<>();
        try ( Connection conn = dataSource.getConnection();
              Statement st = conn.createStatement(); )
        {    
            ResultSet res = st.executeQuery(getQuery("FIND_ALL"));

            while (res.next()) {
                User user = new User();
                user.setUsername(res.getString("username"));
                user.setPassword(res.getString("password"));
                user.setRole(res.getString("role"));
                list.add(user);
            }

        } catch (SQLException e) {
            list = new ArrayList<>();
        }
        
        return list;
    }
    
    public User login(String username, String password) {
        User loggedUser = new User();
         try ( Connection conn = dataSource.getConnection();
              PreparedStatement st = conn.prepareStatement(getQuery("LOGIN")); )
        {
            st.setString(1, username);
            st.setString(2, password);
            ResultSet res = st.executeQuery();
            while (res.next()) {
                // Check the parameters match with friends.jsp form.
                loggedUser.setUsername(res.getString("username"));
                loggedUser.setPassword(res.getString("password"));
                loggedUser.setRole(res.getString("role"));
            }
        } catch (SQLException e) {
            loggedUser = null;
        }
        return loggedUser;
    }

}