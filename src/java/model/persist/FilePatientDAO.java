package model.persist;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import model.Patient;
import model.User;

/**
 * Models a Patient register.
 * @author Amoros
 * @version 1.0, 16/02/2021
 */
public class FilePatientDAO {
    // atributo que será un objeto
    private FilesDataBase d;

    public FilePatientDAO() {
    }

    public FilePatientDAO(String path) {
        d = new FilesDataBase(path + "/files/osteoporosi.csv");
    }
    
    /**
     * Lists all users in database.
     *
     * @return a List of User objects
     */
    public List<Patient> listAll() {
        List<Patient> patients = new ArrayList<>();
        List<String> all = d.listAllLines();

        for (String s : all) {
            // idRegistre;edat;grupEdat;pes;talla;imc;classificacio;menarquia;menopausia;tipus_menopausia;
            String[] pieces = s.split(";");
            User u = new User();

            int id = Integer.parseInt(pieces[0]);
            int age = Integer.parseInt(pieces[1]);
            // Error al leer el símbolo - en CSV.
            String ageGroup = pieces[2].replace("â€“","-");
            int weight = Integer.parseInt(pieces[3]);
            int height = Integer.parseInt(pieces[4]);
            double imc = Double.parseDouble(pieces[5]);
            String classification = pieces[6];
            int menarche = Integer.parseInt(pieces[7]);
            boolean menopause = pieces[8].toUpperCase().equals("SI");
            String menopauseType = pieces[9];
            
            patients.add(new Patient(id, age, ageGroup, weight, height, imc,
                    classification, menarche, menopause, menopauseType));
        }

        return patients;
    }
    
    /**
     * List the use filtered by several criteria.
     *
     * @param classification Accepted terms: normal, osteopenia, osteoporosi
     * @param menopauseType Accepted terms: no consta, natural, ovariectomia, histeroctomia, ambdues
     * @return a List of Patients
     */
    public List<Patient> listFiltered(String classification, String menopauseType) {
        List<Patient> allPatients = listAll();
        List<Patient> filteredPatients = new ArrayList<>();
        boolean filterResultClas = true;
        boolean filterResultMen = true;
        // Si almenys un dels dos filtres está informat, fem 
        // el filtre.
        if(!classification.equals("-")
                || !menopauseType.equals("-"))
        {
            for (Patient p : allPatients) {
                filterResultClas = 
                   classification.equals("-") || 
                      (!classification.equals("-") 
                      && p.getClassification().toUpperCase()
                      .equals(classification.toUpperCase()));
                
                filterResultMen = 
                   menopauseType.equals("-") || 
                      (!menopauseType.equals("-") 
                      && p.getMenopauseType().toUpperCase()
                      .equals(menopauseType.toUpperCase()));
                
                if(filterResultClas && filterResultMen) {
                    filteredPatients.add(p);
                }
            }
        } else {
            filteredPatients = allPatients;
        }
        return filteredPatients;
    }
    
    /**
     * Fa un recompte de la classifiació del número 
     * de pacients. És a dir; quants estan normal, quants tenen osteopenia 
     * i quants osteoporosi
     * @return a Map with
     */
    public Map<String,Double> patientsClasssificationPieChart() {
        Map<String,Double> result 
                = new HashMap<String,Double>();
        
        // The result should be like this:
//        result.put("Normal",6.0);
//        result.put("Oestopenia",3.0);
//        result.put("Oestoporosi",2.0);

        // Steps
        // 1. Get all patients list.
        List<Patient> listPatients = listAll();
        
        // 2. Define names and counters for each patient classification.
        // The solution is not generic, but in this case, 
        // with only 3 categories it's OK
        double cat1NumNormal = 0.0;
        double cat2NumOestopenia = 0.0;
        double cat3NumOestoporosi = 0.0;
        
        String cat1TextNormal = "NORMAL";
        String cat2TextOestopenia = "OSTEOPENIA";
        String cat3TextNormal = "OSTEOPOROSI";
        
        // Aux String with the class. of the patient in the loop.
        String selectedPatientClassification = "";
        
        // 3. Now we loop all the patients and count 
        // the number of patients in each cateogory. 
        for(Patient pat: listPatients) {
            selectedPatientClassification = pat.getClassification();
            switch (selectedPatientClassification) {
                case "NORMAL":
                    cat1NumNormal++;
                    break;
                case "OSTEOPENIA":
                    cat2NumOestopenia++;
                    break;
                case "OSTEOPOROSI":
                    cat3NumOestoporosi++;
                    break;
            }
        }
        
        // 4. At last, we put the results in the Map
        result.put("Normal",cat1NumNormal);
        result.put("Oestopenia",cat2NumOestopenia);
        result.put("Oestoporosi",cat3NumOestoporosi);        
        return result;
    }
}
