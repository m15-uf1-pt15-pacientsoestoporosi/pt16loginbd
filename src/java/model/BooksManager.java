/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author mique
 */
public class BooksManager {
    
    static List<Book> booksList;
    
            
    public BooksManager() {
        booksList = new ArrayList<Book>();
        booksList.add(
            new Book("9788415307594","Feminisme de butxaca2","Feminisme", "Bel Olid", "EBOOK"));
        booksList.add(
            new Book("9788415307594","Follem?","FEMINISME", "Bel Olid", "PAPER"));
        booksList.add(
            new Book("9788416245642","Curs de feminisme per a microones","FEMINISME", "Natja Farré", "PAPER"));
        booksList.add(
            new Book("9788499304212","Caçadora de cossos","EROTICA", "Najat El Hachmi", "PAPER"));
        booksList.add(
            new Book("8789045119533","Wonder","JUVENIL", "Raquel Palacio", "EBOOK"));
        booksList.add(
            new Book("8539045119533","Animals Fantástics i on trobarlos","JUVENIL", "JK Rowklin", "EBOOK"));
        booksList.add(
            new Book("8539045119533","Animals Fantástics i on trobarlos","JUVENIL", "JK Rowklin", "PAPER"));
    }   
        
    public List<String> buscarTodasLasCategorias() {
        // TODO. Esto es una chpuza para hacer una demo.
        List<String> categoria = new ArrayList<String>();
        categoria.add("FEMINISME");
        categoria.add("EROTICA");
        categoria.add("JUVENIL");
        return categoria;
    }
    
    public List<Book> buscarPorCategoria(String categoria) {
//        List<Libro> librosCategoria = new ;
//        for()
        List<Book> filteredBooks = new ArrayList<>();
        
        for(Book b : booksList) {
            if(b.getCategoria().equals(categoria)) {
                filteredBooks.add(b);
            }
        }
        return filteredBooks;
    }

    public static void insertar() {
	
    }

    public List<Book> buscarTodos() {	
        return booksList;
    }
}
