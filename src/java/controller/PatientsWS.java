/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.google.gson.Gson;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.Patient;
import model.persist.FilePatientDAO;


@WebServlet(name = "PatientsWS", urlPatterns = {"/PatientsWS"}, 
        description="Shows all the patients from the study.")
public class PatientsWS extends HttpServlet {
      
     /**
     * Llama a la clase UserDAO de los usuarios de la app.
     */
    private String path;
    private FilePatientDAO patientDAO;
    
    @Override
    public void init(ServletConfig config) throws ServletException{
        super.init(config);
    }
    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        HttpSession session=request.getSession();
        if(session.getAttribute("user")==null){
            JSONResponse(response, "Error! No estàs registrat al servei.");
        } else {

            // calcula la ruta absoluta para llegar a WEB-INF 
            // Cuando hacemos Clean & Build, se genera otra estructura de directorios: LoginApplication/build/web/WEB-INF/
            path = getServletContext().getRealPath("/WEB-INF");
            patientDAO = new FilePatientDAO(path);
            Gson gson = new Gson();

            String action=request.getParameter("action");
            List<Patient> patients = new ArrayList<Patient>();
            switch(action){
                // Exemple: 
                // http://localhost:8080/pt15-oestoporosi-javaweb/PatientsWS?action=List
                case "List":
                    // Llegim els pacients de la base de dades
                    patients = patientDAO.listAll();
                    // Posem la llista de pacients en un fitxer JSON.
                    String patientsJsonString = gson.toJson(patients);
                    // Responem incloent la llista de pacients en format JSON
                    JSONResponse(response, patientsJsonString);
                    break; 
                    
                // Exemple:
                // http://localhost:8080/pt15-oestoporosi-javaweb/PatientsWS?action=FilterByClassification&classification=NORMAL
                case "FilterByClassification":
                    // Llegim la classificació passada per l'usuari. 
                    String classification=request.getParameter("classification");
                    // Llegim els pacients de la base de dades
                    patients = 
                            patientDAO.listFiltered(classification,"-");
                    // Posem la llista de pacients en un fitxer JSON.
                    String patientsJsonStringFilter = gson.toJson(patients);
                    // Responem incloent la llista de pacients en format JSON
                    JSONResponse(response, patientsJsonStringFilter);
                    break; 
            }  
        }
    }

    /**
     * Responem la petició HTTP en format JSON.
     * @param response, HTTP Response
     * @param patientsJsonString, l'String amb la resposta en format JSON.
     * @throws IOException 
     */
    private void JSONResponse(HttpServletResponse response, String patientsJsonString) throws IOException {
        PrintWriter out = response.getWriter();
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        out.print(patientsJsonString);
        out.flush();
    }
    
    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
