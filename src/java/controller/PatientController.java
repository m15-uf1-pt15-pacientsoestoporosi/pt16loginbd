package controller;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import java.util.*;
import javax.servlet.annotation.WebServlet;
import model.Patient;
import model.persist.FilePatientDAO;


@WebServlet(name = "PatientController", urlPatterns = {"/Patients"})
public class PatientController extends HttpServlet {
    
     /**
     * Llama a la clase UserDAO de los usuarios de la app.
     */
    private String path;
    private FilePatientDAO patientDAO;
    
    @Override
    public void init(ServletConfig config) throws ServletException{
        super.init(config);
    }
    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        HttpSession session=request.getSession();
        if(session.getAttribute("user")==null){
            response.sendRedirect("login.jsp"); 
        } 
        // calcula la ruta absoluta para llegar a WEB-INF 
        // Cuando hacemos Clean & Build, se genera otra estructura de directorios: LoginApplication/build/web/WEB-INF/
        path = getServletContext().getRealPath("/WEB-INF");
        patientDAO = new FilePatientDAO(path);

        if(request.getParameter("action")!=null){
            String action=request.getParameter("action");
            switch(action){
                case "List":
                    listPatients(request,response);
                break;
                case "FilterForm":
                    filterForm(request,response);
                break;
                case "filterPatients":
                    filterPatients(request,response);
                break;
                case "graficPatients":
                    pieChartPatientsClassification(request,response);
                break;
                
            }  
        } else{
            response.sendRedirect("login.jsp");
        }
    }
    
    private void listPatients(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // Llegim els pacients de la base de dades
        List<Patient> patients = patientDAO.listAll();
                
        // Li podem enviar la llista de pacients com a Atribut.
        request.setAttribute("patientsList", patients);
        RequestDispatcher rd = request.getRequestDispatcher("patients.jsp");
        rd.forward(request, response);
    }
    
    
    private void filterForm(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
        // Llegim els pacients de la base de dades
        List<Patient> patients = patientDAO.listAll();
        
        // Li podem enviar la llista de pacients com a Atribut.
        request.setAttribute("patientsList", patients);
        RequestDispatcher rd = request.getRequestDispatcher("filter.jsp");
        rd.forward(request, response);
    }
    
    private void filterPatients(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // Llegim els filtres seleccionats.
        String category = request.getParameter("category");
        String menopauseType = request.getParameter("menopauseType"); 
        
        // Llegim els pacients que compleixin els filtres.
        List<Patient> filteredPatients = patientDAO.
                listFiltered(category,menopauseType);
        
        // Li podem enviar la llista de pacients com a Atribut.
        request.setAttribute("patientsList", filteredPatients);
        RequestDispatcher rd = request.getRequestDispatcher("filter.jsp");
        rd.forward(request, response);
    }
    
    
    /**
     * Retorna a la JSP les dades necessàries per a crear un diagrama
     * circular de la classificació de casos dels pacients; quants tenen
     * una situació normal, quants tenen oestopenia i quants tenen oestoporosi.
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException 
     */
    private void pieChartPatientsClassification(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // Llegim els pacients de la base de dades
        Map<String,Double> patientsClasssificationPieChart 
                = patientDAO.patientsClasssificationPieChart();
                
        // Li podem enviar la llista de pacients com a Atribut.
        request.setAttribute("patientsMap", patientsClasssificationPieChart);
        RequestDispatcher rd = request.getRequestDispatcher("grafic2.jsp");
        rd.forward(request, response);
    }
    
    
    
// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
            
    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    
}