/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.User;
import model.persist.DataBaseUserDAO;

/**
 *
 * @author alumne
 */
@WebServlet(name = "UserController", urlPatterns = {"/user"})
public class UserController extends HttpServlet {

   
    /**
     * Llama a la clase UserDAO de los usuarios de la app.
     */
    private String dataBasePath;
    private DataBaseUserDAO userDAODataBase;
    
    @Override
    public void init(ServletConfig config) throws ServletException{
        super.init(config);
    }
    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        // calcula la ruta absoluta para llegar a WEB-INF 
        // Cuando hacemos Clean & Build, se genera otra estructura de directorios: LoginApplication/build/web/WEB-INF/
        dataBasePath = getServletContext().getRealPath("/WEB-INF/resources");
        userDAODataBase = new DataBaseUserDAO(dataBasePath);
        
        if(request.getParameter("action")!=null){
            String action=request.getParameter("action");
            switch(action){
                case "Validate":
                    login(request,response);
                break;
                case "AdminUsers":
                    admin(request,response);
                break;
                case "Invalidate":
                    logout(request,response);
                break;
            }  
        } else{
            response.sendRedirect("login.jsp");
        }
        
        
    }
    
    private void admin(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // Agafem les dades de la sessió.
        HttpSession session=request.getSession();
        if(session.getAttribute("user")==null){
                response.sendRedirect("login.jsp");
         } else {
             if(!session.getAttribute("role").equals("ADMIN")){
                  response.sendRedirect("login.jsp");
             } else {
                // Carreguem la llista d'usuaris completa. 
                List<User> usersList = userDAODataBase.findAll();
                request.setAttribute("usersList", usersList);
                RequestDispatcher rd = request.getRequestDispatcher("admin.jsp");
                rd.forward(request, response);
                // response.sendRedirect("admin.jsp");
            }
        }
    }
        
    private void login(HttpServletRequest request, HttpServletResponse response) throws IOException {
        // Agafem les dades del formulari.
        String username=request.getParameter("username");
        String password=request.getParameter("password"); 
        
        User userForm = new User(username, password);
        
// Si l'usuari amb contrassenya existeix a la nostra base de dades.
        User userDataBase = userDAODataBase.login(username, password);
        if (userDataBase != null) {
            // crear una variable de sesion y asignar el usuario
            HttpSession session = request.getSession();

            //crear una variable de sesSIÓ
            session.setAttribute("user", username);
            // I una cookie
            //setting session to expiry in 30 mins
            session.setMaxInactiveInterval(30*60);
            Cookie userName = new Cookie("user", username);
            userName.setMaxAge(30*60);
            response.addCookie(userName);
            // Indiquem a la vista si l'usuari es admin o user.
            session.setAttribute("role", userDataBase.getRole().toUpperCase());
            response.sendRedirect("welcome.jsp");
        } else {
            response.sendRedirect("login.jsp?error=1");
        }
    }
    
//    private void oldlogin(HttpServletRequest request, HttpServletResponse response) throws IOException {
//        // Agafem les dades del formulari.
//        String username=request.getParameter("username");
//        String password=request.getParameter("password"); 
//        
//        User u = new User(username, password);
//        // Si l'usuari amb contrassenya existeix a la nostra base de dades.
//        // buscar el usuario en el fichero
//        if (udao.validUserAndPassword(u)) {
//            // crear una variable de sesion y asignar el usuario
//            HttpSession session = request.getSession();
//            u = udao.find(username);
//
//            //crear una variable de sesSIÓ
//            session.setAttribute("user", username);
//            // I una cookie
//            //setting session to expiry in 30 mins
//            session.setMaxInactiveInterval(30*60);
//            Cookie userName = new Cookie("user", username);
//            userName.setMaxAge(30*60);
//            response.addCookie(userName);
//            // Indiquem a la vista si l'usuari es admin o user.
//            session.setAttribute("role", u.getRole());
//            response.sendRedirect("welcome.jsp");
//        } else {
//            response.sendRedirect("login.jsp?error=1");
//        }
//    }
    
    private void logout(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/html");
    	Cookie[] cookies = request.getCookies();
    	if(cookies != null){
            for(Cookie cookie : cookies){
                    if(cookie.getName().equals("JSESSIONID")){
                        System.out.println("JSESSIONID="+cookie.getValue());
                        break;
                    }
            }
    	}
    	//invalidate the session if exists
    	HttpSession session = request.getSession(false);
        // Crec que no fa cap falta.
//        session.removeAttribute("user");
//        session.removeAttribute("role");
//        session.invalidate();
    	System.out.println("User="+session.getAttribute("user"));
    	
        response.sendRedirect("login.jsp");
    }
             
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    

}
