package model;

import model.persist.FileUserDAO;
import static junit.framework.Assert.assertEquals;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author mique
 */
public class UsersJUnitTest_1 {
    
    public UsersJUnitTest_1() {
    }

    @BeforeClass
    public static void setUpClass() {
    
    }
    
    @AfterClass
    public static void tearDownClass() {
    
    }
     
    FileUserDAO userDAOTest;
    /**
     * Aquest test s'executarà abans dels demés testos.
     */
    @Before
    public void setUp() {
        // calcula la ruta absoluta para llegar a WEB-INF 
        // Cuando hacemos Clean & Build, se genera otra estructura de directorios: LoginApplication/build/web/WEB-INF/    
        String workingDir = System.getProperty("user.dir");
        userDAOTest = new FileUserDAO(workingDir + "/web/WEB-INF/");
    }
    
    @After
    public void tearDown() {
    
    }
    
    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    @Test
    public void hello() {
        int valorEsperat = 7;
        assertEquals(5 + 2, valorEsperat);
    }
    
    /**
     * Test that if we try to login with a user and/or password that exists in database
     * the method returns true.
     * Preconditions: 
     * There must exist an User with username=dawbio2;password=dawbio2;role=admin;
     */
    @Test
    public void testExistingValidUserAndPassword() {
        User expectedUser = new User("dawbio2","dawbio2");
        boolean expectedResult = true;
        boolean testResult = userDAOTest.validUserAndPassword(expectedUser);
        // String filePath = workingDir + "/web/WEB-INF/files/users.txt";
        assertEquals(expectedResult,testResult);
    }
    
    /**
     * Test that if we try to login with a user and/or password that not exists in database
     * the method returns false.
     * Preconditions: 
     * There must exist an User with username=dawbio2;password=dawbio2;role=admin;
     * 
     */
    @Test
    public void testNotExistingValidUserAndPassword() {
        User expectedUser = new User("user1","1234");
        boolean expectedResult = false;
        boolean testResult = userDAOTest.validUserAndPassword(expectedUser);
        assertEquals(expectedResult,testResult);
    }
    
    /**
     * Test that if we try to login with a user and/or password that exists 
     * in database but the password is incorrect; method returns false.
     * Preconditions: 
     * There must exist an User with username=dawbio1;password=dawbio1;role=basic;
     * 
     */
    @Test
    public void testExistingValidUserIncorrectPassword() {
        User expectedUser = new User("dawbio1","1234");
        boolean expectedResult = false;
        boolean testResult = userDAOTest.validUserAndPassword(expectedUser);
        assertEquals(expectedResult,testResult);
    }
    
    
    /**
     * Test that if we login with a user and/or password who has admin role
     * is, actually, an ADMIN.
     * Preconditions: 
     * There must exist an User with username=dawbio2;password=dawbio2;role=admin;
     */
    @Test
    public void testExistingUserAdminCorrect() {
        User adminUser = new User("dawbio2","dawbio2");
        boolean expectedResult = true;
        boolean testResult = userDAOTest.isAdmin(adminUser);
        // String filePath = workingDir + "/web/WEB-INF/files/users.txt";
        assertEquals(expectedResult,testResult);
    }
    
    /**
     * Test that if we login with a user and/or password who has basic role
     * is, actually, a BASIC.
     * Preconditions: 
     * There must exist an User with username=dawbio2;password=dawbio2;role=admin;
     */
    @Test
    public void testExistingUserBasicCorrect() {
        User expectedUser = new User("dawbio1","dawbio1");
        boolean expectedResult = false;
        boolean testResult = userDAOTest.isAdmin(expectedUser);
        assertEquals(expectedResult,testResult);
    }
    
}
