package model;

import model.persist.FilePatientDAO;
import java.util.List;
import static junit.framework.Assert.assertEquals;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author mique
 */
public class PatientsJUnitTest {
    
    public PatientsJUnitTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    
    }
    
    @AfterClass
    public static void tearDownClass() {
    
    }
     
    FilePatientDAO patientDAOTest;
    /**
     * Aquest test s'executarà abans dels demés testos.
     */
    @Before
    public void setUp() {
        // calcula la ruta absoluta para llegar a WEB-INF 
        // Cuando hacemos Clean & Build, se genera otra estructura de directorios: LoginApplication/build/web/WEB-INF/    
        String workingDir = System.getProperty("user.dir");
        patientDAOTest = new FilePatientDAO(workingDir + "/web/WEB-INF/");
    }
    
    @After
    public void tearDown() {
    
    }
    
    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    @Test
    public void hello() {
        int valorEsperat = 7;
        assertEquals(5 + 2, valorEsperat);
    }
    
    /**
     * Test that we can read successfully the first line of the patients file.
     * Preconditions: 
     * The first patient row should be like this:
     * 1;57;55–59;70;168;24.8;OSTEOPENIA;12;NO;NO CONSTA;
     */
    @Test
    public void testReadExistingFirstPatient() {
        List<Patient> listAllPatients = patientDAOTest.listAll();
        int expectedIdPatient = 1;
        int testIdPatient = listAllPatients.get(0).getRegisterId();
        assertEquals(expectedIdPatient,testIdPatient);
        
        String expectedClassification = "OSTEOPENIA";
        String testClassification = listAllPatients.get(0).getClassification();
        assertEquals(expectedClassification,testClassification);
    }
    
    /**
     * Test that we can read successfully the first line of the patients file.
     * Preconditions: 
     * The first patient row should be like this:
     * 1;57;55–59;70;168;24.8;OSTEOPENIA;12;NO;NO CONSTA;
     */
    @Test
    public void testReadExistingThirdPatient() {
        List<Patient> listAllPatients = patientDAOTest.listAll();
        int expectedIdPatient = 3;
        int testIdPatient = listAllPatients.get(2).getRegisterId();
        assertEquals(expectedIdPatient,testIdPatient);
        
        String expectedClassification = "NORMAL";
        String testClassification = listAllPatients.get(2).getClassification();
        assertEquals(expectedClassification,testClassification);
    }
    
}
